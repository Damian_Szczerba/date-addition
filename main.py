'''
Write a program that will perform date/time addition. Input can be interactive using standard input or command line.
3 parameters are required: a whole number, a unit of time using the following values: year, month, week, day, hour,
minute, second, and a date and time (you choose the date format). The result will be the new date and time.
Example (using ISO 8601 date/time format without time zone designator):

Enter a date and time (YYYY-MM-DDThh:mm:ss): 2012-03-17T09:00:00
Enter value to add: 10
Enter unit of time (year, month, week, day, hour, minute, second): minute
New date and time is 2012-03-17T09:10:00

Enter a date and time (YYYY-MM-DDThh:mm:ss): 2012-11-29T15:00:00
Enter value to add: 5
Enter unit of time (year, month, week, day, hour, minute, second): day
New date and time is 2012-12-04T15:00:00

Enter a date and time (YYYY-MM-DDThh:mm:ss): 2012-06-01T09:00:00
Enter value to add: -2
Enter unit of time (year, month, week, day, hour, minute, second): week
New date and time is 2012-05-18T09:00:00
'''


def is_leap_year(year):
    if year % 4 == 0:
        if year % 100 == 0:
            if year % 400 == 0:
                return True
            else:
                return False
        else:
            return True
    else:
        return False


class AddTime:
    def __init__(self, date, time, value, unit):
        self.year, self.month, self.day = int(date[0]), int(date[1]), int(date[2])
        self.hour, self.minute, self.second = int(time[0]), int(time[1]), int(time[2])
        self.value = value
        self.unit = unit

        old_date = self.formatted_text()

        # Days in each month
        self.dict_days_in_month = {"0": 31, "1": 28, "2": 31, "3": 30,
                                   "4": 31, "5": 30, "6": 31, "7": 31,
                                   "8": 30, "9": 31, "10": 30, "11": 31}

        # If the year is a leap year change the number of days in Feb to 29
        if is_leap_year(self.year):
            self.dict_days_in_month["2"] = 29

        # Call the first function according to the unit (given by the user)
        if self.unit == "second":
            self.method_second()
        elif self.unit == "minute":
            self.method_minute()
        elif self.unit == "hour":
            self.method_hour()
        elif self.unit == "day":
            self.method_day()
        elif self.unit == "month":
            self.method_month()
        elif self.unit == "year":
            self.method_year()

        if self.month == 0:
            self.month = 12

        new_date = self.formatted_text()

        # Display the old and new dates
        print("Old date: " + old_date)
        print("New date: " + new_date)

    def method_second(self):
        # limit is used for checking the limit of this unit (60 because there are 60 seconds in a minute)
        limit = 60

        # time holds the value of how many times to call 'method_minute', as the value is increased by 1 each time
        time = self.calculate_timer("second", limit, self.second)

        # Call self.method_minute 'time' times to increase self.minute by 1 (this may also call other methods)
        for i in range(time):
            self.method_minute()

        self.second = (self.second + self.value) % limit

    def method_minute(self):
        # limit is used for checking the limit of this unit (60 because there are 60 minutes in an hour)
        limit = 60
        if self.unit == "minute":
            # times holds the value of how many times to call 'method_hour', as the value is increased by 1 each times
            times = self.calculate_timer("minute", limit, self.minute)

            # Call self.method_hour 'times' times to increase self.hour by 1 (this may also call other methods)
            for i in range(times):
                self.method_hour()

            self.minute = (self.minute + self.value) % limit
        else:  # Increase self.minute by 1 and get the appropriate result
            if (self.minute + 1) >= limit:
                self.method_hour()
                self.minute = (self.minute + 1) % limit
            else:
                self.minute =+ 1

    def method_hour(self):
        # limit is used for checking the limit of this unit (24 because there are 24 hours in a day)
        limit = 24
        if self.unit == "hour":
            # time holds the value of how many times to call 'method_day', as the value is increased by 1 each time
            time = self.calculate_timer("hour", limit, self.hour)

            # Call self.method_day 'time' times to increase self.day by 1 (this may also call other methods)
            for i in range(time):
                self.method_day()

            self.hour = (self.hour + self.value) % limit
        else:  # Increase self.hour by 1 and get the appropriate result
            if (self.hour + 1) >= limit:
                self.method_day()
                self.hour = (self.hour + 1) % limit
            else:
                self.hour =+ 1

    def method_day(self):
        # limit is used for checking the limit of this unit (check the amount of days this month)
        month = str(self.month - 1)
        limit = self.dict_days_in_month[month]
        if self.unit == "day":
            # time holds the value of how many times to call 'method_month', as the value is increased by 1 each time
            time = self.calculate_timer("day", limit, self.day)

            # Call self.method_month 'time' times to increase self.month by 1 (this may also call other methods)
            for i in range(time):
                self.method_month()

            if (self.day + self.value) % limit == 0:
                self.day = limit
            else:
                self.day = (self.day + self.value) % limit
        else:  # Increase self.day by 1 and get the appropriate result
            if (self.day + 1) > limit:
                self.method_month()
                self.day = (self.day + 1) % limit
            else:
                self.day =+ 1

    def method_month(self):
        # limit is used for checking the limit of this unit (12 because there are 12 months in a year)
        limit = 12
        if self.unit == "month":
            # time holds the value of how many times to call 'method_year', as the value is increased by 1 each time
            time = self.calculate_timer("month", limit, self.month)

            # Call self.method_year 'time' times to increase self.year by 1
            for i in range(time):
                self.method_year()

            self.month = (self.month + self.value) % limit
        else:  # Increase self.month by 1 and get the appropriate result
            if (self.month + 1) > limit:
                self.method_year()
                self.month = (self.month + 1) % limit
            else:
                self.month += 1

    def method_year(self):
        if self.unit == "year":
            self.year += self.value
        else:
            self.year += 1

    def calculate_timer(self, type, limit, unit):
        copy_value = self.value
        time = 0
        if type in ("second", "minute", "hour"):
            while copy_value + unit >= limit:
                copy_value -= limit
                time += 1
        elif type in ("day", "month"):
            while copy_value + unit > limit:
                copy_value -= limit
                time += 1
        return time

    def formatted_text(self):
        return "{}-{:0>2d}-{:0>2d} {:0>2d}:{:0>2d}:{:0>2d}".format(self.year, self.month, self.day,
                                                                   self.hour, self.minute, self.second)
# date_time = str(input("Enter a date and time (YYYY-MM-DD hh:mm:ss):"))
# value = int(input("Enter a value to add: "))
# unit = str(input("Enter unit of time (year, month, week, day, hour, minute, second): "))

date_time = "2014-11-31 23:59:59"
value = 1
unit = "second"

date = date_time.split()[0].split("-")
time = date_time.split()[1].split(":")

AddTime(date, time, value, unit)

